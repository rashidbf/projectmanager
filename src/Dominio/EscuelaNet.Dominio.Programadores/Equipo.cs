﻿using EscuelaNet.Dominio.SeedWoork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Dominio.Programadores
{
    public class Equipo : Entity
    {
        public string Nombre { get; set; }
        public IList<Programador> Programadores { get; set; }
        public IList<Conocimiento> Conocimientos { get; set; }

        public Equipo()
        {
            
        }

        public void PushProgramador(Programador programador)
        {
            foreach (var conocimiento in this.Conocimientos)
            {
                if (programador.Conocimientos == null || programador.Conocimientos.Contains(conocimiento))
                {
                    throw new Exception("No contiene los conocimientos del equipo");
                }
            }
            this.Programadores.Add(programador);
        }

    }
}