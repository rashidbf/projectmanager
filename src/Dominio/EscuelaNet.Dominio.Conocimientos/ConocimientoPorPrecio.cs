﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Dominio.Conocimientos
{
    public class ConocimientoPorPrecio
    {
        public double precio { set; get;}
        public DateTime fecha { set; get;}
        public Nivel nivel { set; get;}
    }
}
