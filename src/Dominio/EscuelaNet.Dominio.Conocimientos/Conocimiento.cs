﻿using EscuelaNet.Dominio.SeedWoork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Dominio.Conocimientos
{
    public class Conocimiento : Entity, IAggregateRoot
    {
        public string Descripcion { get; set; }

        public Disponibilidad Disponibilidad { get; set; }
        public IList<Instructor> Instructores { get; set; }
        public Demanda Demanda { get; set; }

        public IList<ConocimientoPorPrecio> ConocimientoPorprecio { set; get; }
        


        public IList<Archivo> Archivos { get; set; }



        
            
      
    }
}