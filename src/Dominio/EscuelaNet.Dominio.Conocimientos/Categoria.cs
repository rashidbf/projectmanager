﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Dominio.Conocimientos
{
   public class Categoria
    {
        public string Descripcion { get; set; }
        public IList<Conocimiento> Conocimientos { get; set; }

    }
}
