﻿using EscuelaNet.Dominio.Proyectos;
using EscuelaNet.Dominio.SeedWoork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Dominio.Capacitaciones
{
    public class Capacitacion : Entity, IAggregateRoot
    {
        public Capacitacion()
        {

        }

        public Instructor Instructor { get; set; }


        private int Duracion { get; set; }
        public void SetDuracion(int duracion)
        {
            if (duracion <= 0)
                throw new ExcepcionDeProyectos("Error La Duracion de la Capacitacion no puede ser 0");
            Duracion = duracion;
        }
        public int GetDuracion()
        {
            return Duracion;
        }
        public DateTime MomentoInicio { get; set; }
        public IList<Programador> Programadores { get; set; }

        public IList<Alumno> Alumnos { get; set; }

        public void PushProgramador(string nombre)
        {
            if (this.Programadores == null)
            {
                this.Programadores = new List<Programador>();
            }
            this.Programadores.Add(new Programador(nombre));
        }
        //public IList<Conocimientos> ConocimientoId { get; set; }
        




    }
}
